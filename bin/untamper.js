#!/usr/bin/env node
'use strict'

const lockfile = require('@yarnpkg/lockfile')
const fs = require('fs')
const debug = require('debug')('untamper')
const npa = require('npm-package-arg')
const argv = require('minimist')(process.argv.slice(2))
const chalk = require('chalk')

const pkg = require('package-json')

async function check () {
  const filename = argv.l || argv.lockfile || 'yarn.lock'
  debug('checking if %s has been tempered with', filename)
  const file = fs.readFileSync(filename, 'utf8')

  const json = lockfile.parse(file)
  const alert = []

  if (json.type !== 'success') { throw new Error('could not parse lockfile') }

  for (const [fullname, value] of Object.entries(json.object)) {
    debug('checking entry: %s', fullname)
    const name = parsePackageName(fullname)
    const { version, resolved, integrity } = value

    const pkgData = await pkg(name, { allVersions: true })
    const allVersions = Object.keys(pkgData.versions)
    if (!allVersions.includes(version)) {
      debug('version %s specified in the lockfile does not exist in the registry', value.version)
      process.exit(1)
    }

    const dist = pkgData.versions[version].dist

    // expectedIntegrity is a SHA512 hash of the package's tarball. If the SHA512 is not
    // present, it will be a SHA1 hash. In both cases, the hash is base64 encoded.
    //
    // Example:
    // sha512-iRDPJKUPVEND7dHPO8rkbOnPpyDygcDFtWjpeWNCgy8WP2rXcxXL8TskReQl6OrB2G7+UJrags1q15Fudc7G6w==
    // sha1-s2nW+128E+7PUk+RsHD+7cNXzzQ=
    const expectedIntegrity = dist.integrity || _toSha1IntegrityString(dist.shasum)
    if (integrity !== expectedIntegrity) {
      alert.push(`integrity hash of ${fullname} is ${integrity}, but expected ${expectedIntegrity}`)
    }

    // 'yarn info' resolves packages to registry.npmjs.org and doesn't respect --registry flag
    // change the registry manually to yarn's registry.
    const expectedResolved = dist.tarball.replace(
      'https://registry.npmjs.org',
      'https://registry.yarnpkg.com') +
    '#' + dist.shasum

    if (expectedResolved !== resolved) {
      alert.push(`${fullname} is resolved from ${resolved}, but expected ${expectedResolved}`)
    }
  }

  if (alert.length === 0) {
    console.log('Found no signs that the lockfile has been tampered with')
  } else {
    console.log(chalk.red('ERROR') + ' the lockfile has been tampered with:')
    for (const a of alert) {
      console.log('- ' + a)
    }
    process.exit(1)
  }
}

function parsePackageName (fullname) {
  return npa(fullname).name
}

function _toSha1IntegrityString (shasum) {
  return 'sha1-' + Buffer.from(shasum, 'hex').toString('base64')
}

check().catch((err) => console.log(err))
