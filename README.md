A tool for checking if `yarn.lock` was tampered with.

## Usage

```
bin/untamper.js --lockfile yarn.lock
```

Requires yarn v1 to be in your `PATH`.

## Why

The content of `yarn.lock` can be manipulated to resolve a declared dependency to an alternative, attacker-controlled dependencies. This tool prevents such an attack scenario by checking that dependencies in `yarn.lock` are resolved correctly.

## How it works

Each entry in `yarn.lock`  is of the form:
```
dependencyname@semver:
  version: x.y.z
  resolved "https://registry.yarnpkg.com/..."
  integrity ...
```

The tool iterates over each such entry and does:

1. Get all versions of `dependencyname` by calling `yarn info dependencyname versions` 
1. If `version`  does not exist in the versions returned from the registry => alert
1. Query yarn registry for the package metadata with `yarn info dependencyname@version dist`. This returns the integrity and resolve URLs as stored in the registry. 
1. Compare values for integrity and resolved obtained in the previous step with the corresponding values in the lock file. If they don’t match => alert
