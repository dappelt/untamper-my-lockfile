#!/bin/sh -x

bin/untamper.js --lockfile ./test/fixtures/yarn-tampered.lock
[ $? -ne 1 ] && echo 'TEST FAILED' && exit 1

bin/untamper.js --lockfile ./test/fixtures/yarn.lock
[ $? -ne 0 ] && echo 'TEST FAILED' && exit 1

echo 'TEST SUCCEEDED'
